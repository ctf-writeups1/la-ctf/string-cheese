# string-cheese

I'm something of a cheese connoisseur myself. If you can guess my favorite flavor of string cheese, I'll even give you a flag. Of course, since I'm lazy and socially inept, I slapped together a program to do the verification for me.

Connect to my service at nc lac.tf 31131

Note: The attached binary is the exact same as the one executing on the remote server.

file : string_cheese

## Analyse

We have a server. When we connect to him, the serv ask us to send the favorite flavor of cheese.
We have a binary file. When opening in text editor, at the end of bytes, we can see the sentence used by the serv. We also can see a string : blueberry

## Method

Send blueberry to the serv return the flag.